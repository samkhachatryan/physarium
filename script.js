const canvas = document.getElementById('main-canvas');
const ctx = canvas.getContext('2d');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

ctx.fillStyle = "blue";

const agentWidth = 20;
const sensorWidth = 10;
const sensorCenterCoef = (agentWidth - sensorWidth) / 2;
const angleNormalizeCoef = 135;

let agentsCount = 300;
let sensorLength = 20;
let sensorAngle = 90;
let pheromonWidth = 4;
let pheromonLength = 1000;
let showAgent = false;

let cancelFrame = () => { };

(() => {
  document.getElementById('agents-count-input').onchange = (e) => {
    agentsCount = +e.target.value;
    document.getElementById('agents-count').textContent = e.target.value;
    runAnimation();
  }

  document.getElementById('sensor-length-input').onchange = (e) => {
    sensorLength = +e.target.value;
    document.getElementById('sensor-length').textContent = e.target.value;
    runAnimation();
  }

  document.getElementById('sensor-angle-input').onchange = (e) => {
    sensorAngle = +e.target.value;
    document.getElementById('sensor-angle').textContent = e.target.value;
    runAnimation();
  }

  document.getElementById('pheromon-width-input').onchange = (e) => {
    pheromonWidth = +e.target.value;
    document.getElementById('pheromon-width').textContent = e.target.value;
    runAnimation();
  }

  document.getElementById('pheromon-length-input').onchange = (e) => {
    pheromonLength = +e.target.value;
    document.getElementById('pheromon-length').textContent = e.target.value;
    runAnimation();
  }

  document.getElementById('show-agent-input').onchange = (e) => {
    showAgent = +e.target.checked;
  }
})();

const runAnimation = () => {
  cancelFrame();
  const pheromons = {};
  const pheromonPositions = {};

  const calcRotation = (angle, x, y) => {
    const angleRadians = angle * (Math.PI / 180);
    const newX = x * 100 * Math.cos(angleRadians) - y * 100 * Math.sin(angleRadians);
    const newY = x * 100 * Math.sin(angleRadians) + y * 100 * Math.cos(angleRadians);

    return [newX / 100, newY / 100];
  }

  const sensorConfig = (() => {
    const basicPosX = 0;
    const basicPosY = -sensorLength;

    const [firstPosX, firstPosY] = calcRotation(-sensorAngle, basicPosX, basicPosY);
    const [thirdPosX, thirdPosY] = calcRotation(+sensorAngle, basicPosX, basicPosY);

    return [
      {
        posX: firstPosX,
        posY: firstPosY,
        angle: -sensorAngle,
      },
      {
        posX: basicPosX,
        posY: basicPosY,
      },
      {
        posX: thirdPosX,
        posY: thirdPosY,
        angle: sensorAngle,
      }
    ];
  })();

  const getSensorInteraction = (sensor) => {
    for (let x = 0; x < sensorWidth; x++) {
      for (let y = 0; y < sensorWidth; y++) {
        const interactionPosition = `${Math.round(sensor.posX) + x},${Math.round(sensor.posY) - y}`;
        if (pheromonPositions[interactionPosition]) return pheromonPositions[interactionPosition];
      }
    }

    return null;
  };

  let stoped = false;

  const createAgentSensors = (agent) => {
    return sensorConfig.map((config) => {
      const [rotatedX, rotatedY] = calcRotation(agent.angle, config.posX, config.posY);

      const sensorX = agent.posX + rotatedX + sensorCenterCoef;
      const sensorY = agent.posY + rotatedY + sensorCenterCoef;

      return {
        posX: sensorX,
        posY: sensorY,
        angle: config.angle,
      };
    });
  };

  const createAgent = () => {
    const angle = Math.random() * 1000 % 360;

    const agent = {
      angle,
      posX: Math.random() * 10000 % window.innerWidth,
      posY: Math.random() * 10000 % window.innerHeight,
      pheromonColor: `${Math.random() * 1000 % 256}, ${Math.random() * 1000 % 256}, ${Math.random() * 1000 % 256}`,
    };

    agent.sensors = createAgentSensors(agent);

    return agent;
  }

  const initAgents = (count) => {
    const list = [];
    for (let i = 0; i < count; i++) {
      const agent = createAgent();
      agent.id = i;
      list.push(agent);
    }
    return list;
  }

  const agents = initAgents(agentsCount);

  const drawSensors = (agent) => {
    const prevFillStyle = ctx.fillStyle;

    ctx.fillStyle = 'red';

    agent.sensors.forEach(sensor => {
      ctx.fillRect(sensor.posX, sensor.posY, sensorWidth, sensorWidth);
    });

    ctx.fillStyle = prevFillStyle;
  };

  const createPheramon = (agent, outOfMap) => {
    const agentPheromons = pheromons[agent.id] || [];

    if (agentPheromons.length === pheromonLength || (outOfMap && agentPheromons.length)) {
      delete pheromonPositions[`${Math.round(agentPheromons[0].posX)},${Math.round(agentPheromons[0].posY)}`];
      agentPheromons.shift();
    }

    if (outOfMap) return;

    const newPheromon = {
      agentId: agent.id,
      angle: agent.angle,
      posX: agent.posX,
      posY: agent.posY,
      color: agent.pheromonColor,
      getAlpha() {
        const index = agentPheromons.indexOf(newPheromon);

        if (!~index) return 0;

        return index / pheromonLength;
      },
    };

    agentPheromons.push(newPheromon);

    pheromonPositions[`${Math.round(agent.posX)},${Math.round(agent.posY)}`] = newPheromon;

    pheromons[agent.id] = agentPheromons;
  }

  const drawPheromons = (agent) => {
    const agentPheromons = pheromons[agent.id];

    if (agentPheromons) {
      const prevFillStyle = ctx.fillStyle;

      agentPheromons.forEach((pheromon, index) => {
        // ctx.fillStyle = `rgba(255, 252, 127, ${index / pheromonLength})`;
        ctx.fillStyle = `rgba(${pheromon.color}, ${index / pheromonLength})`;
        ctx.fillRect(pheromon.posX, pheromon.posY, pheromonWidth, pheromonWidth);
      });

      ctx.fillStyle = prevFillStyle;
    }
  }

  const drawAgents = () => {
    agents.forEach(agent => {
      if (showAgent) {
        ctx.fillRect(agent.posX, agent.posY, agentWidth, agentWidth);
        drawSensors(agent);
      }
      drawPheromons(agent);
    });
  };

  const moveAgentSensors = (agent) => {
    let bestSensorInteraction = null;

    agent.sensors = createAgentSensors(agent);

    agent.sensors.forEach((sensor) => {
      const interactionPheramon = getSensorInteraction(sensor);

      if (interactionPheramon && interactionPheramon.agentId !== agent.id) {
        const alpha = interactionPheramon.getAlpha();

        if (!bestSensorInteraction || alpha > bestSensorInteraction.alpha)
          bestSensorInteraction = {
            alpha,
            sensorAngle: sensor.angle,
            pheramon: interactionPheramon,
          };
      }
    })

    if (bestSensorInteraction) {
      agent.angle = bestSensorInteraction.pheramon.angle;
      // agent.pheramonColor = bestSensorInteraction.pheramon.color;
      // agent.posX = bestSensorInteraction.pheramon.posX;
      // agent.posY = bestSensorInteraction.pheramon.posY; 
    } else {
      agent.angle = Math.random() * 1000 % 360;
      agent.eating = false;
    }
  }

  const moveAgent = (agent) => {
    let outOfMap = false;
    const [diffX, diffY] = calcRotation(agent.angle - angleNormalizeCoef, pheromonWidth, pheromonWidth);

    if (agent.posX + diffX < 0) {
      agent.angle -= 90;
      outOfMap = true;
    }

    if (agent.posX + diffX > canvas.width) {
      agent.angle += 90;
      outOfMap = true;
    }

    if (agent.posY + diffY < 0) {
      agent.angle += 90
      outOfMap = true;
    }

    if (agent.posY + diffY > canvas.height) {
      agent.angle -= 90;
      outOfMap = true;
    }

    moveAgentSensors(agent);

    if (!outOfMap) {
      agent.posX += diffX;
      agent.posY += diffY;  
    }

    // if (agent.posX > canvas.width || agent.posX < 0 || agent.posY > canvas.height || agent.posY < 0) {
    // agent.angle = 360 - agent.angle;
    // }

    createPheramon(agent, outOfMap);
  }

  const frame = () => {
    ctx.clearRect(0, 0, canvas.width, canvas.height);


    agents.forEach(moveAgent);

    drawAgents();
    // ctx.clearRect(posX, posY, 100, 100);

    // posX += 10;
    // posY += 5;

    // ctx.fillRect(++posX, ++posY, 100, 100);

    if (!stoped) window.requestAnimationFrame(frame);
  }

  cancelFrame = () => window.cancelAnimationFrame(frame);

  window.requestAnimationFrame(frame);
}

runAnimation();